
#include <cassert>
#include <cstdio>
#include <cstring>
#include "Decoder.h"
#include "log.h"

#define min(a, b) \
    ({ __typeof__ (a) _a = (a); \
        __typeof__ (b) _b = (b); \
        _a <= _b ? _a : _b; })

const uint8_t uthernet::barker[2] = {0xca, 0xfe};

uthernet::Decoder::Decoder(uint8_t *buf, size_t size, valid_frame_handler on_frame,
                           frame_error_handler on_error)
        : m_on_frame{on_frame},
          m_on_error{on_error},
          m_current_frame{Frame(nullptr, 0)}
{
    set_buffer(buf, size);
    m_on_frame = on_frame;
    m_on_error = on_error;
}

void uthernet::Decoder::set_buffer(uint8_t *buf, size_t size)
{
    assert(size >= MIN_FRAME_SIZE);
    m_buf = buf;
    m_size = size;
    reset();
}

void uthernet::Decoder::reset()
{
    m_read_head = m_buf;
    m_write_head = m_buf;
    enter_barker_state();
}

void uthernet::Decoder::enter_barker_state()
{
    m_state = BARKER;
    m_required_bytes = sizeof(barker);
    m_current_frame = Frame(nullptr, 0);
}

size_t uthernet::Decoder::feed(const uint8_t *buf, size_t size)
{
    size_t ret = write(buf, size);
    decode();
    return ret;
}

size_t uthernet::Decoder::write(const uint8_t *buf, size_t size)
{
    size_t to_add = min(size, free_space());

    memcpy(m_write_head, buf, to_add);
    m_write_head += to_add;
    print_debug("added %zu/%zu bytes", to_add, size);
    return to_add;
}

void uthernet::Decoder::decode()
{
    while (available_bytes() >= m_required_bytes) {
        print_debug("available data: %zu bytes\n", available_bytes());
        print_debug("required: %zu bytes\n", m_required_bytes);
        switch (m_state) {
            case BARKER:
                process_barker_state();
                break;
            case SIZE:
                process_size_state();
                break;
            case CONTENT_CRC:
                process_content_state();
                break;
            default:
                /* WTF */
                break;
        }
    }
}

void uthernet::Decoder::mark_write(size_t size)
{
    assert(size <= free_space());
    m_write_head += size;
    decode();
}

void uthernet::Decoder::process_barker_state()
{
    size_t missing;

    print_debug("read head: 0x%hhx%hhx\n", m_read_head[0], m_read_head[1]);
    if (m_read_head[0] == barker[0] &&
        m_read_head[1] == barker[1]) {
        print_debug("%s\n", "Barker found");
        realign();
        consume(sizeof(barker));
        enter_size_state();
        return;
    }

    print_debug("%s\n", "Barker not found");
    missing = (m_read_head[1] == barker[0]) ? 1 : sizeof(barker);
    consume(missing);

    if (available_bytes() < m_required_bytes) {
        realign();
    }
}

void uthernet::Decoder::enter_size_state() {
    m_state = uthernet::Decoder::SIZE;
    m_required_bytes = 3; /* Size(2) + HEC(1) */}

void uthernet::Decoder::process_size_state()
{
    uint16_t size;
    uint8_t hec;

    size = htons(*(uint16_t *)(m_read_head));
    hec = *(m_read_head + sizeof(uint16_t));

    if (!verify_size(size, hec)) {
        print_error("%s\n", "Size corrupted beyond repair");
        /**
         * Reset the state, but don't discard any data. As long as the
         * size is not verified, we are still looking for a frame.
         * Maybe the size + hec bytes actually contains the barker of a
         * valid frame.
         */
        enter_barker_state();
        on_error(HEC_ERROR);
        return;
    }
    consume(sizeof(size) + sizeof(hec));
    print_debug("size=%hd, HEC=%hhd\n", size, hec);

    if (size < ROUTE_HEADER_SIZE + CRC_SIZE) {
        /* Incomplete frame */
        print_error("incomplete frame, size=%hd\n", size);
        enter_barker_state();
        on_error(INVALID_SIZE);
        return;
    }

    if (size > free_space() +
               available_bytes()) {
        /* Frame cannot fit in the given buffer */
        print_error("frame too big, size=%hd\n", size);
        enter_barker_state();
        on_error(INVALID_SIZE);
        return;
    }

    enter_content_state(size);
}

void uthernet::Decoder::enter_content_state(uint16_t size) {
    // The `size` from the wire includes only the routing header, payload and
    // CRC. But `Frame::size()` include the entire frame, Barker to CRC
    m_current_frame = uthernet::Frame(m_buf, size + FRAME_HEADER_SIZE);
    m_state = uthernet::Decoder::CONTENT_CRC;
    m_required_bytes = size;
}

void uthernet::Decoder::process_content_state()
{
    uint16_t crc;
    uint16_t size = htons(m_current_frame.size());
    /* When this function is invoked, current_frame has the frame's Size */
    consume(size - CRC_SIZE);
    crc = htons(*(uint16_t *)m_read_head);
    consume(sizeof(crc));

    if (!verify_crc(m_current_frame.content_start(),
                    m_current_frame.size(), crc)) {
        print_error("CRC Error. Received 0x%hx Expected 0x0\n", crc);
        on_error(CRC_ERROR);
        goto out;
    }

    print_debug("%s\n", "valid frame found");
    on_frame();

out:
    enter_barker_state();
}

bool uthernet::Decoder::verify_size(uint16_t &size, uint8_t hec)
{
    return hec == 0; /* TODO: Implement HEC calculation */
}

void uthernet::Decoder::realign()
{
    uint8_t *p = m_read_head;
    size_t size = m_write_head - p;

    if (p == m_buf || !size)
        return;
    reset();
    write(p, size);
}

bool uthernet::Decoder::verify_crc(const uint8_t *buf, size_t size, uint16_t crc) {
    /* TODO: Implement actual CRC check */
    return crc == 0;
}
