#include <arpa/inet.h>

#include <CUnit/CUnit.h>
#include <Decoder.h>

#include "log.h"
#include "frames.h"

using namespace uthernet;

struct frame_attrs {
    uint8_t *payload;
    size_t payload_size;
    uint16_t dst;
    uint16_t src;
    uint8_t flags;
};

static unsigned int s_frame_counter;
static struct frame_attrs s_expected_frames[10];
static enum Decoder::Error s_expected_error = Decoder::NO_ERROR;

static inline void expect_error(enum Decoder::Error error)
{
    s_expected_error = error;
}

static inline void expect_frame(unsigned int index, uint16_t dst, uint16_t src,
         uint8_t flags, uint8_t *payload, size_t payload_size)
{
    s_expected_frames[index].dst = dst;
    s_expected_frames[index].src = src;
    s_expected_frames[index].flags = flags;
    s_expected_frames[index].payload = payload;
    s_expected_frames[index].payload_size = payload_size;
}

static void on_error(Decoder *decoder, Decoder::Error error)
{
    print_error("on error called with error: %d\n", error);
    CU_ASSERT_EQUAL(error, s_expected_error);
}

static void on_frame(Decoder *decoder, const Frame &frame)
{
    struct frame_attrs attrs = s_expected_frames[s_frame_counter];
    s_frame_counter++;

    print_debug("on frame called for frame with payload size: %zu\n",
                frame.payload_size());
    CU_ASSERT_EQUAL(frame.payload_size(), attrs.payload_size);
    CU_ASSERT_EQUAL(frame.dst(), attrs.dst);
    CU_ASSERT_EQUAL(frame.src(), attrs.src);
    CU_ASSERT_EQUAL(frame.flags(), attrs.flags);
}

static void test_bad_crc()
{
    uint8_t buf[MIN_FRAME_SIZE];

    Decoder d(buf, sizeof(buf), nullptr, on_error);

    expect_error(Decoder::CRC_ERROR);
    d.feed(bad_crc_frame, sizeof(bad_crc_frame));
    CU_ASSERT_EQUAL(d.current_state(), Decoder::BARKER);
}

static void test_empty_frame()
{
    uint8_t buf[MAX_FRAME_SIZE];

    s_frame_counter = 0;
    expect_error(Decoder::NO_ERROR);
    expect_frame(0, htons(0xaabb), htons(0xccdd), 0xe, nullptr, 0);
    expect_frame(1, htons(0xaabb), htons(0xccdd), 0xe, nullptr, 0);

    Decoder d(buf, sizeof(buf), on_frame, on_error);


    d.feed(empty_frame, sizeof(empty_frame));
    CU_ASSERT_EQUAL(s_frame_counter, 1);
    d.feed(empty_frame_offset, sizeof(empty_frame_offset));
    CU_ASSERT_EQUAL(s_frame_counter, 2);
}

static void test_several_frames_in_buffer()
{
    uint8_t buf[MAX_FRAME_SIZE];
    uint8_t payload[] = {0x11, 0x22, 0x33, 0x44};

    expect_error(Decoder::NO_ERROR);
    Decoder d(buf, sizeof(buf), on_frame, on_error);
    expect_frame(0, htons(0xaabb), htons(0xccdd), 0xe, payload, sizeof(payload));
    expect_frame(1, htons(0xaabb), htons(0xccdd), 0xe, payload, sizeof(payload));

    s_frame_counter = 0;
    d.feed(two_frames, sizeof(two_frames));
    CU_ASSERT_EQUAL(s_frame_counter, 2);

    s_frame_counter = 0;
    d.feed(two_adjacent_frames, sizeof(two_adjacent_frames));
    CU_ASSERT_EQUAL(s_frame_counter, 2);
}

void test_correct_frame_analyzing()
{
    uint8_t buf[] = {
            0xca, 0xfe, 0x0, 0x14, 0x0, /* frame header */
            0xaa, 0xaa, 0xbb, 0xbb, 0xaa, /* route header */
            0x80, 0x80, 0x80, 0x80, 0x80, 0x7f, /* extension bytes */
            0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, /* payload */
            0x0, 0x0 /* CRC */

    };

    Frame frame(buf, sizeof(buf));

    CU_ASSERT_EQUAL(ntohs(frame.size()), 0x14);
    CU_ASSERT_EQUAL(frame.dst(), (uint16_t)0xaaaa);
    CU_ASSERT_EQUAL(frame.src(), (uint16_t)0xbbbb);
    CU_ASSERT_EQUAL(frame.flags(), 0xaa);
    CU_ASSERT_EQUAL(frame.has_extension(), true);
    CU_ASSERT_EQUAL(frame.is_urgent(), false);
    CU_ASSERT_EQUAL(frame.packet_type(), 2);
    CU_ASSERT_EQUAL(frame.next_protocol(), 10);
    CU_ASSERT_EQUAL(frame.extension_bytes(), 6);
    CU_ASSERT_EQUAL(frame.extension(), buf + 10);
    CU_ASSERT_EQUAL(frame.payload_size(), 7);
    CU_ASSERT_EQUAL(frame.payload(), buf + FRAME_HEADER_SIZE + ROUTE_HEADER_SIZE + 6);
    CU_ASSERT_EQUAL(frame.crc(), 0);
}

void test_flags_parsing()
{
    uint8_t buf[MIN_FRAME_SIZE];

    Frame frame(buf, sizeof(buf));

    frame.set_has_extension(true);
    frame.set_urgent(false);
    frame.set_packet_type(1);
    frame.set_next_protocol(10);

    CU_ASSERT_EQUAL(frame.flags(), 0x9a);
}



CU_TestInfo frame_tests[] = {
    {"Bad CRC", test_bad_crc},
    {"Empty frame", test_empty_frame},
    {"Several Frames in Buffer", test_several_frames_in_buffer},
    {"Correct frame analyzing", test_correct_frame_analyzing},
    {"Correct frame assignment", test_flags_parsing},
    CU_TEST_INFO_NULL
};
