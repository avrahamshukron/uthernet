#ifndef UTHERNET_DECODER_H
#define UTHERNET_DECODER_H

#include <cstdint>
#include <cstddef>
#include <cassert>

#include "Frame.h"


namespace uthernet {

    extern const uint8_t barker[2];

    class Decoder {

    public:
        enum Error {
            NO_ERROR = 0,
            INVALID_SIZE,
            HEC_ERROR,
            CRC_ERROR,
        };

        enum State {
            BARKER,
            SIZE,
            CONTENT_CRC,
        };

        typedef void (*valid_frame_handler)
                (Decoder *decoder, const Frame &frame);

        typedef void (*frame_error_handler)
                (Decoder *decoder, enum Error error);

    private:
        enum State m_state;
        size_t m_size;
        size_t m_required_bytes;
        uint8_t *m_buf;
        uint8_t *m_read_head;
        uint8_t *m_write_head;
        valid_frame_handler m_on_frame;
        frame_error_handler m_on_error;
        struct Frame m_current_frame;

    public:

        /**
         * Initialize decoder object.
         *
         * @param buf: A buffer that will be shared between the application and
         *  the decoder. The application is expected to _put_ data in the
         *  buffer, while the decoder will _scan_ the buffer in search for a
         *  frame.
         * @param size: The buffer size. The size must be at least
         *  MIN_FRAME_SIZE.
         * @param on_frame: A callback function that will be invoked when a
         *  valid frame will be detected by the decoder.
         * @param on_error: A callback that will be invoked by the decoder if
         *  some error occur during the decode process.
         */
        Decoder(uint8_t *buf, size_t size,
                        valid_frame_handler on_frame,
                        frame_error_handler on_error = nullptr);

        void set_buffer(uint8_t *buf, size_t size);

        /**
         * Resets the decoder to its initial state. All existing data will be
         * discarded.
         */
        void reset();

        /**
         * Appends data to the decoder's buffer.
         *
         * @param decoder: The decoder.
         * @param buf: A buffer containing the data to add to the decoder.
         * @param size: How many bytes from `buf` to add.
         *
         * @return: How many bytes were actually added. If there is not enough
         *  space in the decoder's buffer, not all the data will be added.
         */
        size_t feed(const uint8_t *buf, size_t size);

        /**
         * Tells the decoder that `size` bytes have been written to its buffer.
         * Application needs to call this method if they wrote data directly to
         * the buffer using a pointer returned from `Decoder::write_head`.
         * Calling this method will cause the decoder to process the new data.
         *
         * @param size: How many bytes were added to the buffer.
         *
         * @see Decoder::write_head
         */
        void mark_write(size_t size);

        /**
         * @return A pointer to the decoder's buffer.
         */
        uint8_t *buffer() const { return m_buf; }

        /**
         * @return A pointer to the decoder's read head. The read head points
         *  to the next byte in the buffer that is yet to be processed.
         */
        uint8_t *read_head() const { return m_read_head; }

        /**
         * @return A pointer to the decoder's write head.
         *
         * @brief
         *  The write head points to the start of the free space in the
         *  decoder's buffer. An application that wants to write directly to
         *  the decoder's buffer must write only to the pointer returned from
         *  this function.
         *
         * @attention
         *  Writing to anywhere but `write_head` will result in undefined
         *  behavior.
         *  Writing more than `free_space` will cause a Buffer Overrun and an
         *  undefined behavior.
         *
         * @see Decoder::free_space
         * @see Decoder::read_next
         */
        uint8_t *write_head() const { return m_write_head; }

        /**
         * @return The free space in the decoder's buffer.
         *
         * @attention When writing directly to write_head, NEVER write more than
         *  free_space() bytes. The recommended way is to read read_next() bytes
         *  every time, as it is safe and will reduce system calls and buffer
         *  realignments.
         *
         * @see Decoder::read_next
         */
        size_t free_space() const {return (size_t)((m_buf + m_size) - m_write_head);}

        /**
         * @return How many bytes the decoder currently needs.
         *
         * This method is meant to be used as a hint for the application.
         * It tells how many bytes the decoder currently needs.
         * For example, if the application reads from a socket directly into the
         * decoder, then it should call:
         *     recv(fd, decoder.write_head(), decoder.read_next())
         *
         * Using this method will ensure the integrity of the buffer and will
         * reduce the number of system calls and memory copy operations.
         */
        size_t read_next() const { return m_required_bytes - available_bytes(); }

        /**
         * @return The current state of the decoder.
         */
        enum State current_state() { return m_state; }

    private:
        size_t available_bytes() const { return (size_t)(m_write_head - m_read_head); }
        size_t write(const uint8_t *buf, size_t size);
        void decode();

        void enter_barker_state();
        void enter_size_state();
        void enter_content_state(uint16_t size);

        void process_barker_state();
        void process_size_state();
        void process_content_state();

        void realign();
        void consume(size_t size)
        {
            assert(size <= available_bytes());
            m_read_head += size;
        }
        bool verify_size(uint16_t &size, uint8_t hec);
        bool verify_crc(const uint8_t *buf, size_t size, uint16_t crc);

        void on_error(enum Error error)
        {
            if (m_on_error)
                m_on_error(this, error);
        }

        void on_frame()
        {
            if (m_on_frame)
                m_on_frame(this, m_current_frame);
        }
    };
}

#endif //UTHERNET_DECODER_H
