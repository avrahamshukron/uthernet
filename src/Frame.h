#ifndef UTHERNET_FRAME_H
#define UTHERNET_FRAME_H

#include <cstdint>
#include <cstddef>
#include <arpa/inet.h>

#define MASK(width) (~(~0 << (width)))

static inline uint8_t read_bits(uint8_t var, uint8_t msb_pos, uint8_t width)
{
    const int shift = msb_pos - (width - 1);

    return static_cast<uint8_t>((var >> shift) & MASK(width));
}

static inline uint8_t write_bits(uint8_t var, uint8_t msb_pos, uint8_t width, uint8_t value)
{
    const int shift = msb_pos - (width - 1);

    return static_cast<uint8_t>(var & ~(MASK(width) << shift)) | (value << shift);
}


#define EXTENSION_BIT(byte) read_bits(byte, 7, 1)


namespace uthernet {

    const size_t CRC_SIZE = sizeof(uint16_t);
    const size_t FRAME_HEADER_SIZE = 5; /* Barker, Size HEC */
    const size_t ROUTE_HEADER_SIZE = 5; /* DST, SRC, Flags */
    const size_t MIN_FRAME_SIZE =
            (FRAME_HEADER_SIZE + ROUTE_HEADER_SIZE + CRC_SIZE);
    const size_t MAX_FRAME_SIZE = 1600;

    class Frame {

    private:
        uint8_t *m_start;
        size_t m_frame_size;
        uint16_t m_extension_bytes = 0;

        void init();

    public:
        Frame();

        Frame(uint8_t *start, size_t frame_size);

        Frame(const Frame &frame);

        Frame &operator=(const Frame &frame);

        /**
         * @return
         *  A pointer to the start of the frame (The first character of Barker)
         */
        uint8_t *start() const { return m_start; }

        /**
         * @return
         *  A pointer to the start of the frame's content (The first byte of
         *  DST field)
         */
        uint8_t *content_start() const { return m_start + FRAME_HEADER_SIZE; }

        /**
         * @return The total size of the frame (including CRC)
         */
        size_t frame_size() const { return m_frame_size; }

        /**
         * @return A reference to the Size field of the frame header.
         *
         * @note Should be Big-Endian, and it is the application responsibility
         *  to convert it to and from the host's byte-order.
         */
        uint16_t &size() const { return (uint16_t &) (*(uint16_t *) (m_start + 2)); }

        /**
         * @return A reference to the DST field.
         *
         * @note Should be Big-Endian, and it is the application responsibility
         *  to convert it to and from the host's byte-order.
         */
        uint16_t &dst() const { return (uint16_t &) (*(uint16_t *) (m_start + FRAME_HEADER_SIZE)); }

        /**
         * @return A reference to the SRC field.
         *
         * @note Should be Big-Endian, and it is the application responsibility
         *  to convert it to and from the host's byte-order.
         */
        uint16_t &src() const { return (uint16_t &) (*(uint16_t *) (m_start + FRAME_HEADER_SIZE + 2)); }

        /**
         * @return A reference to the flags field.
         */
        uint8_t flags() const { return m_start[FRAME_HEADER_SIZE + 4]; }

        void set_flags(uint8_t flags) const { m_start[FRAME_HEADER_SIZE + 4] = flags; }

        bool has_extension() const { return read_bits(flags(), 7, 1); }

        void set_has_extension(bool has_extension) {
            set_flags(write_bits(flags(), 7, 1, static_cast<uint8_t>(has_extension)));
        }

        bool is_urgent() const { return read_bits(flags(), 6, 1); }

        void set_urgent(bool urgent) const { set_flags(write_bits(flags(), 6, 1, static_cast<uint8_t>(urgent))); }

        uint8_t packet_type() const { return static_cast<uint8_t >(read_bits(flags(), 5, 2)); }

        void set_packet_type(uint8_t pt) const {
            set_flags(write_bits(flags(), 5, 2, pt));
        }

        uint8_t next_protocol() const { return static_cast<uint8_t >(read_bits(flags(), 3, 4)); }

        void set_next_protocol(uint8_t np) const {
            set_flags(write_bits(flags(), 3, 4, np));
        }

        /**
         * @return A pointer to the beginning of the payload section in the
         *  frame.
         */
        uint8_t *payload() const { return m_start + FRAME_HEADER_SIZE + ROUTE_HEADER_SIZE + m_extension_bytes; }

        /**
         * @return The payload size.
         */
        size_t payload_size() const { return ntohs(size()) - CRC_SIZE - ROUTE_HEADER_SIZE - m_extension_bytes; }

        /**
         * @return The number of extension bytes in the frame.
         */
        uint16_t extension_bytes() const { return m_extension_bytes; }

        /**
         * @return A pointer to the first extension byte, if there are any,
         *  null otherwise.
         *
         * @see Frame::extension_bytes
         */
        uint8_t *extension() const {
            return m_extension_bytes > 0 ?
                   m_start + FRAME_HEADER_SIZE + ROUTE_HEADER_SIZE :
                   nullptr;
        }

        uint16_t &crc() const
        { return (uint16_t &) (*(uint16_t *) (m_start + m_frame_size - CRC_SIZE)); }

    };
}

#endif //UTHERNET_FRAME_H
